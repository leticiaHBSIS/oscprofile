import React from 'react';
import './BannerStyle.css';
import bannervoa from "../bannervoa.png"

function Banner() {
  return (
<div className="banner">
    <img className="banner_voa" src={bannervoa}/>
</div>
    );
}
export default Banner;
