import React, { Component } from 'react';
import './MainStyle.css';
import iconface from './assets/icon-face.svg'
import iconinsta from './assets/icon-insta.svg'
import iconyt from './assets/icon-youtube.svg'

function Main() {
  return (
<div className="main" >
    <section className="video_sectiion">
      
        <div className="video_ong">
            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/rqbkJn9kUv4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div className="infosOng">
        <div>
            <p className="NomeOsc">Pisada do Sertão</p>
            <p className="CidadeOsc">Sorocaba-SP</p>
            <input className='categoria' value="cultura" type="button"></input>
        </div>
        <div className="redesOng">
            <img src={iconface}/>
            <img src={iconinsta}/>
            <img src={iconyt}/>
        </div>
        </div>
    </section>

    <div className="resumo_ong">
        <h2 className="titulo">Lorem ipsum</h2>
            <p className="resumo">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
                pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                culpa qui officia deserunt mollit anim id est laborum.
            </p>
        <div className="button_vote">
            <input type="submit" className="vote" value="VOTAR"/>
        </div>
    </div>
 
    
</div>

    );
}

export default Main;
