import React from 'react';
import Header from './Header/Header'
import './App.css';
import Banner from './Body/Banner';
import Main from './Body/Main';
import Footer from './Footer/Footer';

function App() {
  return (
    <div className="App">
       <Header/>
       <Banner/>
       <Main/>
       <Footer/>
    </div>
  );
}

export default App;
