import React from 'react';
import './HeaderStyle.css';
import logovoa from "../logovoa.png";

function Header() {
    return (
    
            <nav className="Navbar" role="navigation">
                <div className="menuToggle">
                <img className="logo_ambev_header" src={logovoa} />
                <div>
                    <input type="checkbox" />
                    <span></span>
                    <span></span>
                    <span></span>
                
                    <ul className="menu">
                        <a target="_blank" href="https://www.ambev.com.br/voa/quem-somos"><li>QUEM SOMOS</li></a>
                        <a href="#"><li>COMO FUNCIONA</li></a>
                        <a href="#"><li>REGULAMENTO</li></a>
                    </ul>
                </div>
                </div>
            </nav>
    );
}

export default Header;
